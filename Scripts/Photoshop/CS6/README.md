# README #

This README explains how to install the Ingress Banner Mission creator script, for Photoshop CS6 

### What is this repository for? ###

* By just typing in a the number of missions your banner will contain, you will get a Photoshop file with, guides, slices and a overlay for preview.

After installing the script, simply sellect file->script->Ingress Banner Templates" and type in the number of missions in your banner.

### How do I get set up? ###

#### Manual installation ####
* [Download this repo](https://bitbucket.org/HuXDK/ingress-banner-mission-template/downloads), unzip
* Locate /scripts/Photoshop/CS6/Ingress Banner template.jsx
* Copy the file to:
* Mac: <hard drive>/Applications/Adobe Photoshop CS6/Presets/Scripts/
* PC: C:\Program Files\Adobe\Adobe Photoshop CS6\Presets\Scripts\

#### Git installation ####
* [Set up Git](https://help.github.com/articles/set-up-git/)
* #### OSX ####
* All in Terminal
* Find a folder (Or make one) you want to store the repo
* In terminal git "fetch && git checkout master"
* Create a symbolic link in terminal "ln -s <location of the repo>/Scripts/Photoshop/CS6/Ingress\ Banner\ templates.jsx /Applications/Adobe Photoshop\ CS6/Presets/Scripts/Ingress\ Banner\ templates.jsx"


### Contribution guidelines ###

* It's a free for all, but please fork, and push updates.

### Who do I talk to? ###

* Michael Hughson (Ingress IGN @HuXdk)